import jwt from "jsonwebtoken";
import { OrderStatus } from "../service";
import createOrder from "./createOrder";
//import createOrder from "./createOrder";
import getMenu from "./getMenu";
import getOrder from "./getOrder";
import getOrders from "./getOrders";
import updateOrderStatus from "./updateOrderStatus";

async function run() {
  try {
    const token = jwt.sign(
      {
        role: "customer",
        username: "mackarelfish",
      },
      "catkeyboard"
    );

    const menu = await getMenu(token);
    console.log(menu);

    const orders = await getOrders(token);
    console.log(orders);

    if (orders.length !== 0) {
      /* const systemToken = jwt.sign(
        { username: "budi.anduk.system", role: "system" },
        "catkeyboard"
      );
      const orderId = orders[0].id
      const updatedOrder = await updateOrderStatus(systemToken, orderId, OrderStatus.PENDING_APPROVAL) */

      const chefToken = jwt.sign(
        { username: "budi.anduk.chef", role: "chef" },
        "catkeyboard"
      );
      const allOrders = await getOrders(chefToken)
      console.log(allOrders)
    } else {
      const createdOrder = await createOrder(token, [
        {
          name: "Pizza Keren",
          quantity: 5,
        },
      ]);
      console.log(createdOrder);
    }
  } catch (e) {
    throw e;
  }
}

run();
