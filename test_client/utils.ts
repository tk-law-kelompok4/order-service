import { credentials } from "grpc"
import { OrderServiceClient } from "../proto/orderservice_grpc_pb"

const port = 50053

export const client = new OrderServiceClient(
    `localhost:${port}`,
    credentials.createInsecure()
);

export const noop = () => {};