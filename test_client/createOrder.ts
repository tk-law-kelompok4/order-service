import {
  CreateOrderReply,
  CreateOrderRequest,
  ItemWithQuantity,
} from "../proto/orderservice_pb";
import { client } from "./utils";

function transformItemWithQuantityObjectToClass({
  name,
  quantity,
}: ItemWithQuantity.AsObject): ItemWithQuantity {
  const item = new ItemWithQuantity();
  item.setName(name);
  item.setQuantity(quantity);
  return item;
}

export default function createOrder(
  accessToken: string,
  itemWithQuanitityList: ItemWithQuantity.AsObject[]
) {
  return new Promise<CreateOrderReply.AsObject>((resolve, reject) => {
    const request = new CreateOrderRequest();
    request.setAccesstoken(accessToken);
    request.setItemsList(
      itemWithQuanitityList.map(transformItemWithQuantityObjectToClass)
    );

    client.createOrder(request, (err, createOrderReply) => {
      if (err) {
        reject(err);
      } else {
        resolve(createOrderReply.toObject());
      }
    });
  });
}
