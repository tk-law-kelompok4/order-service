import { GetOrdersRequest, Order } from "../proto/orderservice_pb";
import { client } from "./utils";

export default function getOrders(accessToken: string) {
  return new Promise<Order.AsObject[]>((resolve, reject) => {
    const request = new GetOrdersRequest();
    request.setAccesstoken(accessToken);

    client.getOrders(request, (err, getOrdersReply) => {
      if (err) {
        reject(err);
      } else {
        console.log(getOrdersReply.getMessage())
        console.log(getOrdersReply.getStatus())
        resolve(getOrdersReply.getDataList().map(transformOrderClassToObject));
      }
    });
  });
}

function transformOrderClassToObject(order: Order): Order.AsObject {
    return order.toObject()
}