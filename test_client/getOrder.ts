import { GetOrderRequest, Order } from "../proto/orderservice_pb";
import { client } from "./utils";

export default function getOrder(accessToken: string, orderId: string) {
  return new Promise<Order.AsObject | null>((resolve, reject) => {
    const request = new GetOrderRequest();
    request.setAccesstoken(accessToken);
    request.setOrderid(orderId)

    client.getOrder(request, (err, getOrderReply) => {
      if (err) {
        reject(err);
      } else {
        console.log(getOrderReply.getMessage())
        console.log(getOrderReply.getStatus())
        const order = getOrderReply.getOrder()
        if (order === undefined) {
            resolve(null)
        } else {
            resolve(order.toObject())
        }
      }
    });
  });
}