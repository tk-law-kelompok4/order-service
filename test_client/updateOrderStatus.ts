import { Order, UpdateOrderRequest } from "../proto/orderservice_pb";
import { OrderStatus } from "../service";
import { client } from "./utils";

export default function updateOrderStatus(
  accessToken: string,
  orderId: string,
  orderStatus: OrderStatus
) {
  return new Promise<Order.AsObject | null>((resolve, reject) => {
    const request = new UpdateOrderRequest();
    request.setAccesstoken(accessToken);
    request.setOrderid(orderId);
    request.setOrderstatus(orderStatus);

    client.updateOrder(request, (err, updateOrderReply) => {
      if (err) {
        reject(err);
      } else {
        console.log(updateOrderReply.getStatus());
        console.log(updateOrderReply.getMessage());
        const order = updateOrderReply.getOrder();
        if (order !== undefined) {
          resolve(order.toObject());
        } else {
          resolve(null);
        }
      }
    });
  });
}
