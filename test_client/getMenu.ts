import { Item, MenuRequest } from "../proto/orderservice_pb";
import { client } from "./utils";

export default function getMenu(accessToken: string) {
  return new Promise<Item.AsObject[]>((resolve, reject) => {
    const request = new MenuRequest();
    request.setAccesstoken(accessToken);

    client.menu(request, (err, menuReply) => {
      if (err) {
        reject(err);
      } else {
        const menu = menuReply.getItemsList().map((item) => item.toObject());
        resolve(menu);
      }
    });
  });
}
