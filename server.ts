import { Server, ServerCredentials } from "grpc";
import { OrderGrpcController } from "./controller";
import {
  IOrderServiceServer,
  OrderServiceService,
} from "./proto/orderservice_grpc_pb";

import { OrderService } from "./service";

const server = new Server();

const orderService = new OrderService();
server.addService<IOrderServiceServer>(
  OrderServiceService,
  new OrderGrpcController(orderService)
);

const port = 50053;
const uri = `localhost:${port}`;
console.log(`Listening on ${uri}`);
server.bind(uri, ServerCredentials.createInsecure());
server.start();
