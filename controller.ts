import {
  credentials,
  sendUnaryData,
  ServerUnaryCall,
  ServiceError,
} from "grpc";
import jwt from "jsonwebtoken";
import { CookingClient, ICookingClient } from "./proto/cooking_grpc_pb";
import { CookRequest } from "./proto/cooking_pb";
import { DeliveryClient, IDeliveryClient } from "./proto/delivery_grpc_pb";
import { DeliverRequest } from "./proto/delivery_pb";
import { IOrderServiceServer } from "./proto/orderservice_grpc_pb";
import {
  MenuRequest,
  MenuReply,
  Item,
  CreateOrderReply,
  CreateOrderRequest,
  GetOrderReply,
  GetOrderRequest,
  GetOrdersReply,
  GetOrdersRequest,
  UpdateOrderReply,
  UpdateOrderRequest,
  OrderStatus,
  ItemOrder,
  Order,
} from "./proto/orderservice_pb";

import {
  IOrderService,
  Item as ServiceItem,
  OrderStatus as ServiceOrderStatus,
  UserRole as ServiceUserRole,
  UserData as ServiceUserData,
  OrderedItemWithPrice,
  OrderWithPrice,
} from "./service";

function getMessageFromError(error: any): string {
  if (error instanceof Error) {
    return error.message;
  } else {
    return "Unexpected error.";
  }
}

export class OrderGrpcController implements IOrderServiceServer {
  private orderService: IOrderService;
  private cookingClient = new CookingClientHelper();
  private deliveryClient = new DeliveryClientHelper();

  static INVALID_TOKEN_ERROR: ServiceError = {
    message: "Token missing or invalid.",
    name: "Invalid Token",
  };

  constructor(orderService: IOrderService) {
    this.orderService = orderService;
  }

  private isTokenValid(accessToken: string) {
    return true;
  }

  private getUserDataFromToken(accessToken: string): ServiceUserData | null {
    if (!this.isTokenValid(accessToken)) {
      return null;
    }

    const decodedToken = jwt.decode(accessToken, { json: true });
    if (!decodedToken) {
      return null;
    }

    const { role, username } = decodedToken;

    return {
      role: this.transformUserRoleStringToServiceUserRole(role),
      username,
    };
  }

  private transformServiceItemToItem({ name, price }: ServiceItem): Item {
    const item = new Item();
    item.setName(name);
    item.setPrice(price);
    return item;
  }

  private transformServiceOrderStatusToOrderStatus(
    orderStatus: ServiceOrderStatus
  ): OrderStatus {
    switch (orderStatus) {
      case ServiceOrderStatus.COOKING:
        return OrderStatus.COOKING;
      case ServiceOrderStatus.FINISHED:
        return OrderStatus.FINISHED;
      case ServiceOrderStatus.PENDING_APPROVAL:
        return OrderStatus.PENDING_APPROVAL;
      case ServiceOrderStatus.PENDING_PAYMENT:
        return OrderStatus.PENDING_PAYMENT;
      case ServiceOrderStatus.SENDING:
        return OrderStatus.SENDING;
      default:
        throw new Error("Order status invalid.");
    }
  }

  private makeMenuReply(menu: ServiceItem[]): MenuReply {
    const transformedMenu = menu.map((serviceItem) =>
      this.transformServiceItemToItem(serviceItem)
    );
    const menuReply = new MenuReply();
    menuReply.setItemsList(transformedMenu);
    menuReply.setStatus(200);
    menuReply.setMessage("Success.");
    return menuReply;
  }

  menu(call: ServerUnaryCall<MenuRequest>, callback: sendUnaryData<MenuReply>) {
    const userData = this.getUserDataFromToken(call.request.getAccesstoken());
    if (!userData) {
      callback(OrderGrpcController.INVALID_TOKEN_ERROR, null);
      return;
    }

    try {
      const menu = this.orderService.getMenu();
      const menuReply = this.makeMenuReply(menu);
      callback(null, menuReply);
    } catch (error) {
      callback(
        {
          name: "Error",
          message: getMessageFromError(error),
        },
        null
      );
    }
  }

  private transformUserRoleStringToServiceUserRole(
    userRole: string
  ): ServiceUserRole {
    switch (userRole) {
      case "chef":
        return ServiceUserRole.CHEF;
      case "customer":
        return ServiceUserRole.CUSTOMER;
      case "system":
        return ServiceUserRole.SYSTEM;
      default:
        throw new Error("User role invalid.");
    }
  }

  private transformOrderedItemToItemOrder({
    name,
    price,
    quantity,
  }: OrderedItemWithPrice): ItemOrder {
    const itemOrder = new ItemOrder();
    itemOrder.setName(name);
    itemOrder.setPrice(price);
    itemOrder.setQuantity(quantity);
    return itemOrder;
  }

  private makeCreateOrderReply({ id, orderedItems, status }: OrderWithPrice) {
    const transformedOrderedItems = orderedItems.map((orderedItem) =>
      this.transformOrderedItemToItemOrder(orderedItem)
    );
    const createOrderReply = new CreateOrderReply();
    createOrderReply.setId(id);
    createOrderReply.setItemsList(transformedOrderedItems);
    createOrderReply.setOrderstatus(status);
    createOrderReply.setMessage("Success.");
    createOrderReply.setStatus(200);
    return createOrderReply;
  }

  createOrder(
    call: ServerUnaryCall<CreateOrderRequest>,
    callback: sendUnaryData<CreateOrderReply>
  ) {
    const userData = this.getUserDataFromToken(call.request.getAccesstoken());
    if (!userData) {
      callback(OrderGrpcController.INVALID_TOKEN_ERROR, null);
      return;
    }

    try {
      const itemsOrdered = call.request
        .getItemsList()
        .map((item) => item.toObject());
      const createdOrder = this.orderService.createOrder(
        itemsOrdered,
        userData
      );
      const createOrderReply = this.makeCreateOrderReply(createdOrder);
      callback(null, createOrderReply);
    } catch (error) {
      callback(
        {
          message: getMessageFromError(error),
          name: "Error",
        },
        null
      );
    }
  }

  private transformOrderWithPriceToOrder({
    id,
    orderedItems,
    status,
  }: OrderWithPrice): Order {
    const transformedOrderedItems = orderedItems.map((orderedItem) =>
      this.transformOrderedItemToItemOrder(orderedItem)
    );
    const transformedOrderStatus =
      this.transformServiceOrderStatusToOrderStatus(status);
    const order = new Order();
    order.setId(id);
    order.setItemsList(transformedOrderedItems);
    order.setOrderstatus(transformedOrderStatus);
    return order;
  }

  private makeGetOrderReply(order: OrderWithPrice): GetOrderReply {
    const transformedOrder = this.transformOrderWithPriceToOrder(order);
    const getOrderReply = new GetOrderReply();
    getOrderReply.setMessage("Success.");
    getOrderReply.setStatus(200);
    getOrderReply.setOrder(transformedOrder);
    return getOrderReply;
  }

  getOrder(
    call: ServerUnaryCall<GetOrderRequest>,
    callback: sendUnaryData<GetOrderReply>
  ) {
    const userData = this.getUserDataFromToken(call.request.getAccesstoken());
    if (!userData) {
      callback(OrderGrpcController.INVALID_TOKEN_ERROR, null);
      return;
    }

    const orderId = call.request.getOrderid();
    try {
      const order = this.orderService.getOrder(orderId, userData);

      const getOrderReply = this.makeGetOrderReply(order);
      callback(null, getOrderReply);
    } catch (error) {
      callback({ message: getMessageFromError(error), name: "Error" }, null);
    }
  }

  private makeGetOrdersReply(orders: OrderWithPrice[]): GetOrdersReply {
    const transformedOrders = orders.map((order) =>
      this.transformOrderWithPriceToOrder(order)
    );
    const getOrdersReply = new GetOrdersReply();
    getOrdersReply.setMessage("Success.");
    getOrdersReply.setStatus(200);
    getOrdersReply.setDataList(transformedOrders);
    return getOrdersReply;
  }

  getOrders(
    call: ServerUnaryCall<GetOrdersRequest>,
    callback: sendUnaryData<GetOrdersReply>
  ) {
    const userData = this.getUserDataFromToken(call.request.getAccesstoken());
    if (!userData) {
      callback(OrderGrpcController.INVALID_TOKEN_ERROR, null);
      return;
    }

    try {
      const orders = this.orderService.getOrders(userData);

      const getOrdersReply = this.makeGetOrdersReply(orders);
      callback(null, getOrdersReply);
    } catch (error) {
      callback({ message: getMessageFromError(error), name: "Error" }, null);
    }
  }

  private transformOrderStatusStringToServiceOrderStatus(
    orderStatus: string
  ): ServiceOrderStatus {
    switch (orderStatus) {
      case ServiceOrderStatus.COOKING:
        return ServiceOrderStatus.COOKING;
      case ServiceOrderStatus.FINISHED:
        return ServiceOrderStatus.FINISHED;
      case ServiceOrderStatus.PENDING_APPROVAL:
        return ServiceOrderStatus.PENDING_APPROVAL;
      case ServiceOrderStatus.PENDING_PAYMENT:
        return ServiceOrderStatus.PENDING_PAYMENT;
      case ServiceOrderStatus.SENDING:
        return ServiceOrderStatus.SENDING;
      default:
        throw new Error("Order status invalid.");
    }
  }

  private makeUpdateOrderReply(order: OrderWithPrice): UpdateOrderReply {
    const transformedOrder = this.transformOrderWithPriceToOrder(order);
    const updateOrderReply = new UpdateOrderReply();
    updateOrderReply.setMessage("Success.");
    updateOrderReply.setStatus(200);
    updateOrderReply.setOrder(transformedOrder);
    return updateOrderReply;
  }

  updateOrder(
    call: ServerUnaryCall<UpdateOrderRequest>,
    callback: sendUnaryData<UpdateOrderReply>
  ) {
    const accessToken = call.request.getAccesstoken();
    const userData = this.getUserDataFromToken(accessToken);
    if (!userData) {
      callback(OrderGrpcController.INVALID_TOKEN_ERROR, null);
      return;
    }

    const orderId = call.request.getOrderid();

    let orderStatus;
    try {
      orderStatus = this.transformOrderStatusStringToServiceOrderStatus(
        call.request.getOrderstatus()
      );
    } catch (error) {
      callback(
        { message: getMessageFromError(error), name: "Invalid Order Status" },
        null
      );
      return;
    }

    try {
      const updatedOrder = this.orderService.updateOrderStatus(
        orderId,
        orderStatus,
        userData
      );

      if (orderStatus === ServiceOrderStatus.COOKING) {
        this.cookingClient
          .cook(accessToken, orderId, updatedOrder.orderedItems)
          .then((_message) => {
            const updateOrderReply = this.makeUpdateOrderReply(updatedOrder);
            callback(null, updateOrderReply);
          })
          .catch((e: ServiceError) => {
            callback(e, null);
          });
      } else if (orderStatus === ServiceOrderStatus.SENDING) {
        this.deliveryClient
          .deliver(accessToken, orderId)
          .then((_message) => {
            const updateOrderReply = this.makeUpdateOrderReply(updatedOrder);
            callback(null, updateOrderReply);
          })
          .catch((e: ServiceError) => {
            callback(e, null);
          });
      } else {
        const updateOrderReply = this.makeUpdateOrderReply(updatedOrder);
        callback(null, updateOrderReply);
      }
    } catch (error) {
      callback({ message: getMessageFromError(error), name: "Error" }, null);
    }
  }
}

class DeliveryClientHelper {
  private client: IDeliveryClient;

  constructor() {
    const port = 50070;
    this.client = new DeliveryClient(
      `localhost:${port}`,
      credentials.createInsecure()
    );
  }

  deliver(accessToken: string, orderId: string) {
    return new Promise<string>((resolve, reject) => {
      const request = new DeliverRequest();
      request.setAccesstoken(accessToken);
      request.setOrderid(orderId);

      this.client.deliver(request, (err, deliveryReply) => {
        if (err) {
          reject(err);
        } else {
          resolve(deliveryReply.getMessage());
        }
      });
    });
  }
}

class CookingClientHelper {
  private client: ICookingClient;

  constructor() {
    const port = 50060;
    this.client = new CookingClient(
      `localhost:${port}`,
      credentials.createInsecure()
    );
  }

  private transformItemObjectToItem({
    name,
    price,
  }: CookRequest.Item.AsObject): CookRequest.Item {
    const item = new CookRequest.Item();
    item.setName(name);
    item.setPrice(price);
    return item;
  }

  cook(
    accessToken: string,
    orderId: string,
    items: CookRequest.Item.AsObject[]
  ) {
    return new Promise<string>((resolve, reject) => {
      const request = new CookRequest();
      request.setAccesstoken(accessToken);
      request.setOrderid(orderId);
      request.setItemsList(
        items.map((item) => this.transformItemObjectToItem(item))
      );

      this.client.cook(request, (err, cookReply) => {
        if (err) {
          reject(err);
        } else {
          resolve(cookReply.getMessage());
        }
      });
    });
  }
}
