import { v4 as uuidv4 } from "uuid";

export type Item = {
  name: string;
  price: number;
};

export enum OrderStatus {
  PENDING_PAYMENT,
  PENDING_APPROVAL,
  COOKING,
  SENDING,
  FINISHED,
}

export type Order = {
  id: string;
  username: string;
  status: OrderStatus;
  orderedItems: OrderedItem[];
};

export type OrderedItem = {
  name: string;
  quantity: number;
};

export interface IOrderRepository {
  getMenu(): Item[];
  getItemByName(itemName: string): Item;
  createOrder(orderedItems: OrderedItem[], username: string): string;
  getOrdersByUsername(username: string): Order[];
  getOrders(): Order[];
  getOrderById(orderId: string): Order;
  setOrderStatus(orderId: string, orderStatus: OrderStatus): void;
}

const seedMenu: Item[] = [
  { name: "Pizza Keren", price: 200000.0 },
  { name: "Pizza Kere Hore", price: 1000.0 },
  { name: "Pizza Mantap", price: 100000.0 },
  { name: "Pizza Meat Lover", price: 5000.0 },
];

export class InMemoryOrderRepository implements IOrderRepository {
  menu: Map<string, Item> = new Map();
  orders: Map<string, Order> = new Map();

  constructor(menu?: Item[]) {
    const addedMenu = menu ?? seedMenu;
    for (const item of addedMenu) {
      const name = item.name;
      this.menu.set(name, item);
    }
  }

  getMenu(): Item[] {
    return Array.from(this.menu.values());
  }

  getItemByName(itemName: string): Item {
    if (!this.menu.has(itemName)) {
      throw new Error(`${itemName} does not exist on the menu.`);
    }
    return this.menu.get(itemName)!;
  }

  private generateOrderId() {
    return uuidv4();
  }

  private getFormattedInvalidItemNames(items: OrderedItem[]): string {
    const invalidItemName = items
      .filter(({ name }) => !this.menu.has(name))
      .map(({ name }) => name);
    return invalidItemName.join(", ")
  }

  createOrder(orderedItems: OrderedItem[], username: string): string {
    const invalidItemNames = this.getFormattedInvalidItemNames(orderedItems)
    if (invalidItemNames.length > 0) {
      throw new Error(
        `${invalidItemNames} does not exist on the menu.`
      );
    }

    const id = this.generateOrderId();
    this.orders.set(id, {
      id,
      username,
      status: OrderStatus.PENDING_PAYMENT,
      orderedItems,
    });

    return id;
  }

  getOrdersByUsername(username: string): Order[] {
    return Array.from(this.orders.values()).filter(
      ({ username: orderUsername }) => orderUsername === username
    );
  }

  getOrders(): Order[] {
    return Array.from(this.orders.values());
  }

  getOrderById(orderId: string): Order {
    if (!this.orders.has(orderId)) {
      throw new Error("An order with that id does not exist.");
    }
    return this.orders.get(orderId)!;
  }

  setOrderStatus(orderId: string, orderStatus: OrderStatus) {
    if (!this.orders.has(orderId)) {
      throw new Error("An order with that id does not exist.");
    }
    let order = this.orders.get(orderId)!;
    order.status = orderStatus;
  }
}
