import {
  InMemoryOrderRepository,
  IOrderRepository,
  Order,
  OrderedItem,
  OrderStatus as RepositoryOrderStatus,
  Item as RepositoryItem,
} from "./repository";

export enum OrderStatus {
  PENDING_PAYMENT = "pending_payment",
  PENDING_APPROVAL = "pending_approval",
  COOKING = "cooking",
  SENDING = "sending",
  FINISHED = "finished",
}

export type OrderWithPrice = {
  id: string;
  status: OrderStatus;
  orderedItems: OrderedItemWithPrice[];
};

export type OrderedItemWithPrice = {
  name: string;
  quantity: number;
  price: string;
};

export enum UserRole {
  CUSTOMER,
  CHEF,
  SYSTEM,
}

export type UserData = {
  username: string;
  role: UserRole;
};

export type Item = {
  name: string;
  price: string;
};

export interface IOrderService {
  getMenu(): Item[];
  createOrder(orderedItems: OrderedItem[], userData: UserData): OrderWithPrice;
  getOrders(userData: UserData): OrderWithPrice[];
  getOrder(orderId: string, userData: UserData): OrderWithPrice;
  updateOrderStatus(
    orderId: string,
    status: OrderStatus,
    userData: UserData
  ): OrderWithPrice;
}

export class OrderService implements IOrderService {
  private orderRepository: IOrderRepository;

  constructor(orderRepository?: IOrderRepository) {
    this.orderRepository = orderRepository ?? new InMemoryOrderRepository();
  }

  private transformRepositoryItemToItem({ name, price }: RepositoryItem): Item {
    return {
      name,
      price: price.toFixed(2),
    };
  }

  getMenu(): Item[] {
    return this.orderRepository
      .getMenu()
      .map((item) => this.transformRepositoryItemToItem(item));
  }

  private transformRepositoryOrderStatusToOrderStatus(
    orderStatus: RepositoryOrderStatus
  ): OrderStatus {
    switch (orderStatus) {
      case RepositoryOrderStatus.COOKING:
        return OrderStatus.COOKING;
      case RepositoryOrderStatus.FINISHED:
        return OrderStatus.FINISHED;
      case RepositoryOrderStatus.PENDING_APPROVAL:
        return OrderStatus.PENDING_APPROVAL;
      case RepositoryOrderStatus.PENDING_PAYMENT:
        return OrderStatus.PENDING_PAYMENT;
      case RepositoryOrderStatus.SENDING:
        return OrderStatus.SENDING;
      default:
        throw new Error("Order status invalid.");
    }
  }

  private transformOrderStatusToRepositoryOrderStatus(
    orderStatus: OrderStatus
  ): RepositoryOrderStatus {
    switch (orderStatus) {
      case OrderStatus.COOKING:
        return RepositoryOrderStatus.COOKING;
      case OrderStatus.FINISHED:
        return RepositoryOrderStatus.FINISHED;
      case OrderStatus.PENDING_APPROVAL:
        return RepositoryOrderStatus.PENDING_APPROVAL;
      case OrderStatus.PENDING_PAYMENT:
        return RepositoryOrderStatus.PENDING_PAYMENT;
      case OrderStatus.SENDING:
        return RepositoryOrderStatus.SENDING;
      default:
        throw new Error("Order status invalid.");
    }
  }

  private transformOrderToOrderWithPrice({
    id,
    orderedItems,
    status,
  }: Order): OrderWithPrice {
    const orderedItemsWithPrice = orderedItems.map(({ name, quantity }) => {
      const { price } = this.orderRepository.getItemByName(name);
      const formattedPrice = price.toFixed(2);
      return { name, price: formattedPrice, quantity };
    });
    const transformedStatus =
      this.transformRepositoryOrderStatusToOrderStatus(status);
    return {
      id,
      status: transformedStatus,
      orderedItems: orderedItemsWithPrice,
    };
  }

  createOrder(orderedItems: OrderedItem[], userData: UserData): OrderWithPrice {
    if (userData.role !== UserRole.CUSTOMER) {
      throw Error("Only a customer can order.");
    }

    const createdOrderId = this.orderRepository.createOrder(
      orderedItems,
      userData.username
    );
    const createdOrder = this.orderRepository.getOrderById(createdOrderId);
    return this.transformOrderToOrderWithPrice(createdOrder);
  }

  getOrders(userData: UserData): OrderWithPrice[] {
    let orders: Order[];
    switch (userData.role) {
      case UserRole.CUSTOMER:
        orders = this.orderRepository.getOrdersByUsername(userData.username);
        break;
      case UserRole.CHEF:
        orders = this.orderRepository
          .getOrders()
          .filter(
            ({ status }) => status === RepositoryOrderStatus.PENDING_APPROVAL
          );
        break;
      default:
        throw new Error("Role not found.");
    }

    return orders.map((order) => this.transformOrderToOrderWithPrice(order));
  }

  getOrder(orderId: string, userData: UserData): OrderWithPrice {
    const order = this.orderRepository.getOrderById(orderId);
    switch (userData.role) {
      case UserRole.CUSTOMER:
        if (order.username !== userData.username) {
          throw new Error("Unauthorized.");
        }
        break;
      case UserRole.CHEF:
        break;
      default:
        throw new Error("Role not found.");
    }

    return this.transformOrderToOrderWithPrice(order);
  }

  updateOrderStatus(
    orderId: string,
    status: OrderStatus,
    userData: UserData
  ): OrderWithPrice {
    switch (userData.role) {
      case UserRole.CUSTOMER:
        throw new Error("Unauthorized.");
      case UserRole.CHEF:
        if (status !== OrderStatus.COOKING) {
          throw new Error(
            "A chef can only change order status from pending approval to cooking."
          );
        }
        break;
      case UserRole.SYSTEM:
        break;
      default:
        throw new Error("Role not found.");
    }
    this.orderRepository.setOrderStatus(
      orderId,
      this.transformOrderStatusToRepositoryOrderStatus(status)
    );
    const updatedOrder = this.orderRepository.getOrderById(orderId);
    return this.transformOrderToOrderWithPrice(updatedOrder);
  }
}
