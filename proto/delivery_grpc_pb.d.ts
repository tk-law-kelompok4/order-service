// package: delivery
// file: delivery.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "grpc";
import * as delivery_pb from "./delivery_pb";

interface IDeliveryService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    deliver: IDeliveryService_IDeliver;
}

interface IDeliveryService_IDeliver extends grpc.MethodDefinition<delivery_pb.DeliverRequest, delivery_pb.DeliverReply> {
    path: "/delivery.Delivery/Deliver";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<delivery_pb.DeliverRequest>;
    requestDeserialize: grpc.deserialize<delivery_pb.DeliverRequest>;
    responseSerialize: grpc.serialize<delivery_pb.DeliverReply>;
    responseDeserialize: grpc.deserialize<delivery_pb.DeliverReply>;
}

export const DeliveryService: IDeliveryService;

export interface IDeliveryServer {
    deliver: grpc.handleUnaryCall<delivery_pb.DeliverRequest, delivery_pb.DeliverReply>;
}

export interface IDeliveryClient {
    deliver(request: delivery_pb.DeliverRequest, callback: (error: grpc.ServiceError | null, response: delivery_pb.DeliverReply) => void): grpc.ClientUnaryCall;
    deliver(request: delivery_pb.DeliverRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: delivery_pb.DeliverReply) => void): grpc.ClientUnaryCall;
    deliver(request: delivery_pb.DeliverRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: delivery_pb.DeliverReply) => void): grpc.ClientUnaryCall;
}

export class DeliveryClient extends grpc.Client implements IDeliveryClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public deliver(request: delivery_pb.DeliverRequest, callback: (error: grpc.ServiceError | null, response: delivery_pb.DeliverReply) => void): grpc.ClientUnaryCall;
    public deliver(request: delivery_pb.DeliverRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: delivery_pb.DeliverReply) => void): grpc.ClientUnaryCall;
    public deliver(request: delivery_pb.DeliverRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: delivery_pb.DeliverReply) => void): grpc.ClientUnaryCall;
}
