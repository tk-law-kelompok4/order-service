// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var delivery_pb = require('./delivery_pb.js');

function serialize_delivery_DeliverReply(arg) {
  if (!(arg instanceof delivery_pb.DeliverReply)) {
    throw new Error('Expected argument of type delivery.DeliverReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_delivery_DeliverReply(buffer_arg) {
  return delivery_pb.DeliverReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_delivery_DeliverRequest(arg) {
  if (!(arg instanceof delivery_pb.DeliverRequest)) {
    throw new Error('Expected argument of type delivery.DeliverRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_delivery_DeliverRequest(buffer_arg) {
  return delivery_pb.DeliverRequest.deserializeBinary(new Uint8Array(buffer_arg));
}


var DeliveryService = exports.DeliveryService = {
  deliver: {
    path: '/delivery.Delivery/Deliver',
    requestStream: false,
    responseStream: false,
    requestType: delivery_pb.DeliverRequest,
    responseType: delivery_pb.DeliverReply,
    requestSerialize: serialize_delivery_DeliverRequest,
    requestDeserialize: deserialize_delivery_DeliverRequest,
    responseSerialize: serialize_delivery_DeliverReply,
    responseDeserialize: deserialize_delivery_DeliverReply,
  },
};

exports.DeliveryClient = grpc.makeGenericClientConstructor(DeliveryService);
