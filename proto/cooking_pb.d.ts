// package: cooking
// file: cooking.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class CookRequest extends jspb.Message { 
    getAccesstoken(): string;
    setAccesstoken(value: string): CookRequest;
    getOrderid(): string;
    setOrderid(value: string): CookRequest;
    clearItemsList(): void;
    getItemsList(): Array<CookRequest.Item>;
    setItemsList(value: Array<CookRequest.Item>): CookRequest;
    addItems(value?: CookRequest.Item, index?: number): CookRequest.Item;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CookRequest.AsObject;
    static toObject(includeInstance: boolean, msg: CookRequest): CookRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CookRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CookRequest;
    static deserializeBinaryFromReader(message: CookRequest, reader: jspb.BinaryReader): CookRequest;
}

export namespace CookRequest {
    export type AsObject = {
        accesstoken: string,
        orderid: string,
        itemsList: Array<CookRequest.Item.AsObject>,
    }


    export class Item extends jspb.Message { 
        getName(): string;
        setName(value: string): Item;
        getPrice(): string;
        setPrice(value: string): Item;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Item.AsObject;
        static toObject(includeInstance: boolean, msg: Item): Item.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Item, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Item;
        static deserializeBinaryFromReader(message: Item, reader: jspb.BinaryReader): Item;
    }

    export namespace Item {
        export type AsObject = {
            name: string,
            price: string,
        }
    }

}

export class CookReply extends jspb.Message { 
    getStatus(): number;
    setStatus(value: number): CookReply;
    getMessage(): string;
    setMessage(value: string): CookReply;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CookReply.AsObject;
    static toObject(includeInstance: boolean, msg: CookReply): CookReply.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CookReply, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CookReply;
    static deserializeBinaryFromReader(message: CookReply, reader: jspb.BinaryReader): CookReply;
}

export namespace CookReply {
    export type AsObject = {
        status: number,
        message: string,
    }
}
