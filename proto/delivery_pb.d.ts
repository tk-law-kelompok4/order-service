// package: delivery
// file: delivery.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class DeliverRequest extends jspb.Message { 
    getAccesstoken(): string;
    setAccesstoken(value: string): DeliverRequest;
    getOrderid(): string;
    setOrderid(value: string): DeliverRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DeliverRequest.AsObject;
    static toObject(includeInstance: boolean, msg: DeliverRequest): DeliverRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DeliverRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DeliverRequest;
    static deserializeBinaryFromReader(message: DeliverRequest, reader: jspb.BinaryReader): DeliverRequest;
}

export namespace DeliverRequest {
    export type AsObject = {
        accesstoken: string,
        orderid: string,
    }
}

export class DeliverReply extends jspb.Message { 
    getStatus(): number;
    setStatus(value: number): DeliverReply;
    getMessage(): string;
    setMessage(value: string): DeliverReply;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DeliverReply.AsObject;
    static toObject(includeInstance: boolean, msg: DeliverReply): DeliverReply.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DeliverReply, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DeliverReply;
    static deserializeBinaryFromReader(message: DeliverReply, reader: jspb.BinaryReader): DeliverReply;
}

export namespace DeliverReply {
    export type AsObject = {
        status: number,
        message: string,
    }
}
