// package: cooking
// file: cooking.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "grpc";
import * as cooking_pb from "./cooking_pb";

interface ICookingService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    cook: ICookingService_ICook;
}

interface ICookingService_ICook extends grpc.MethodDefinition<cooking_pb.CookRequest, cooking_pb.CookReply> {
    path: "/cooking.Cooking/Cook";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<cooking_pb.CookRequest>;
    requestDeserialize: grpc.deserialize<cooking_pb.CookRequest>;
    responseSerialize: grpc.serialize<cooking_pb.CookReply>;
    responseDeserialize: grpc.deserialize<cooking_pb.CookReply>;
}

export const CookingService: ICookingService;

export interface ICookingServer {
    cook: grpc.handleUnaryCall<cooking_pb.CookRequest, cooking_pb.CookReply>;
}

export interface ICookingClient {
    cook(request: cooking_pb.CookRequest, callback: (error: grpc.ServiceError | null, response: cooking_pb.CookReply) => void): grpc.ClientUnaryCall;
    cook(request: cooking_pb.CookRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: cooking_pb.CookReply) => void): grpc.ClientUnaryCall;
    cook(request: cooking_pb.CookRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: cooking_pb.CookReply) => void): grpc.ClientUnaryCall;
}

export class CookingClient extends grpc.Client implements ICookingClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public cook(request: cooking_pb.CookRequest, callback: (error: grpc.ServiceError | null, response: cooking_pb.CookReply) => void): grpc.ClientUnaryCall;
    public cook(request: cooking_pb.CookRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: cooking_pb.CookReply) => void): grpc.ClientUnaryCall;
    public cook(request: cooking_pb.CookRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: cooking_pb.CookReply) => void): grpc.ClientUnaryCall;
}
