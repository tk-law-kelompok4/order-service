// package: orderservice
// file: orderservice.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Item extends jspb.Message { 
    getName(): string;
    setName(value: string): Item;
    getPrice(): string;
    setPrice(value: string): Item;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Item.AsObject;
    static toObject(includeInstance: boolean, msg: Item): Item.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Item, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Item;
    static deserializeBinaryFromReader(message: Item, reader: jspb.BinaryReader): Item;
}

export namespace Item {
    export type AsObject = {
        name: string,
        price: string,
    }
}

export class ItemWithQuantity extends jspb.Message { 
    getName(): string;
    setName(value: string): ItemWithQuantity;
    getQuantity(): number;
    setQuantity(value: number): ItemWithQuantity;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ItemWithQuantity.AsObject;
    static toObject(includeInstance: boolean, msg: ItemWithQuantity): ItemWithQuantity.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ItemWithQuantity, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ItemWithQuantity;
    static deserializeBinaryFromReader(message: ItemWithQuantity, reader: jspb.BinaryReader): ItemWithQuantity;
}

export namespace ItemWithQuantity {
    export type AsObject = {
        name: string,
        quantity: number,
    }
}

export class ItemOrder extends jspb.Message { 
    getName(): string;
    setName(value: string): ItemOrder;
    getPrice(): string;
    setPrice(value: string): ItemOrder;
    getQuantity(): number;
    setQuantity(value: number): ItemOrder;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ItemOrder.AsObject;
    static toObject(includeInstance: boolean, msg: ItemOrder): ItemOrder.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ItemOrder, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ItemOrder;
    static deserializeBinaryFromReader(message: ItemOrder, reader: jspb.BinaryReader): ItemOrder;
}

export namespace ItemOrder {
    export type AsObject = {
        name: string,
        price: string,
        quantity: number,
    }
}

export class Order extends jspb.Message { 
    getId(): string;
    setId(value: string): Order;
    getOrderstatus(): OrderStatus;
    setOrderstatus(value: OrderStatus): Order;
    clearItemsList(): void;
    getItemsList(): Array<ItemOrder>;
    setItemsList(value: Array<ItemOrder>): Order;
    addItems(value?: ItemOrder, index?: number): ItemOrder;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Order.AsObject;
    static toObject(includeInstance: boolean, msg: Order): Order.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Order, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Order;
    static deserializeBinaryFromReader(message: Order, reader: jspb.BinaryReader): Order;
}

export namespace Order {
    export type AsObject = {
        id: string,
        orderstatus: OrderStatus,
        itemsList: Array<ItemOrder.AsObject>,
    }
}

export class MenuRequest extends jspb.Message { 
    getAccesstoken(): string;
    setAccesstoken(value: string): MenuRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): MenuRequest.AsObject;
    static toObject(includeInstance: boolean, msg: MenuRequest): MenuRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: MenuRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): MenuRequest;
    static deserializeBinaryFromReader(message: MenuRequest, reader: jspb.BinaryReader): MenuRequest;
}

export namespace MenuRequest {
    export type AsObject = {
        accesstoken: string,
    }
}

export class GetOrdersRequest extends jspb.Message { 
    getAccesstoken(): string;
    setAccesstoken(value: string): GetOrdersRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetOrdersRequest.AsObject;
    static toObject(includeInstance: boolean, msg: GetOrdersRequest): GetOrdersRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetOrdersRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetOrdersRequest;
    static deserializeBinaryFromReader(message: GetOrdersRequest, reader: jspb.BinaryReader): GetOrdersRequest;
}

export namespace GetOrdersRequest {
    export type AsObject = {
        accesstoken: string,
    }
}

export class GetOrderRequest extends jspb.Message { 
    getAccesstoken(): string;
    setAccesstoken(value: string): GetOrderRequest;
    getOrderid(): string;
    setOrderid(value: string): GetOrderRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetOrderRequest.AsObject;
    static toObject(includeInstance: boolean, msg: GetOrderRequest): GetOrderRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetOrderRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetOrderRequest;
    static deserializeBinaryFromReader(message: GetOrderRequest, reader: jspb.BinaryReader): GetOrderRequest;
}

export namespace GetOrderRequest {
    export type AsObject = {
        accesstoken: string,
        orderid: string,
    }
}

export class CreateOrderRequest extends jspb.Message { 
    getAccesstoken(): string;
    setAccesstoken(value: string): CreateOrderRequest;
    clearItemsList(): void;
    getItemsList(): Array<ItemWithQuantity>;
    setItemsList(value: Array<ItemWithQuantity>): CreateOrderRequest;
    addItems(value?: ItemWithQuantity, index?: number): ItemWithQuantity;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CreateOrderRequest.AsObject;
    static toObject(includeInstance: boolean, msg: CreateOrderRequest): CreateOrderRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CreateOrderRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CreateOrderRequest;
    static deserializeBinaryFromReader(message: CreateOrderRequest, reader: jspb.BinaryReader): CreateOrderRequest;
}

export namespace CreateOrderRequest {
    export type AsObject = {
        accesstoken: string,
        itemsList: Array<ItemWithQuantity.AsObject>,
    }
}

export class UpdateOrderRequest extends jspb.Message { 
    getAccesstoken(): string;
    setAccesstoken(value: string): UpdateOrderRequest;
    getOrderid(): string;
    setOrderid(value: string): UpdateOrderRequest;
    getOrderstatus(): string;
    setOrderstatus(value: string): UpdateOrderRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): UpdateOrderRequest.AsObject;
    static toObject(includeInstance: boolean, msg: UpdateOrderRequest): UpdateOrderRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: UpdateOrderRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): UpdateOrderRequest;
    static deserializeBinaryFromReader(message: UpdateOrderRequest, reader: jspb.BinaryReader): UpdateOrderRequest;
}

export namespace UpdateOrderRequest {
    export type AsObject = {
        accesstoken: string,
        orderid: string,
        orderstatus: string,
    }
}

export class MenuReply extends jspb.Message { 
    getStatus(): number;
    setStatus(value: number): MenuReply;
    getMessage(): string;
    setMessage(value: string): MenuReply;
    clearItemsList(): void;
    getItemsList(): Array<Item>;
    setItemsList(value: Array<Item>): MenuReply;
    addItems(value?: Item, index?: number): Item;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): MenuReply.AsObject;
    static toObject(includeInstance: boolean, msg: MenuReply): MenuReply.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: MenuReply, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): MenuReply;
    static deserializeBinaryFromReader(message: MenuReply, reader: jspb.BinaryReader): MenuReply;
}

export namespace MenuReply {
    export type AsObject = {
        status: number,
        message: string,
        itemsList: Array<Item.AsObject>,
    }
}

export class CreateOrderReply extends jspb.Message { 
    getStatus(): number;
    setStatus(value: number): CreateOrderReply;
    getMessage(): string;
    setMessage(value: string): CreateOrderReply;
    getId(): string;
    setId(value: string): CreateOrderReply;
    getOrderstatus(): string;
    setOrderstatus(value: string): CreateOrderReply;
    clearItemsList(): void;
    getItemsList(): Array<ItemOrder>;
    setItemsList(value: Array<ItemOrder>): CreateOrderReply;
    addItems(value?: ItemOrder, index?: number): ItemOrder;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CreateOrderReply.AsObject;
    static toObject(includeInstance: boolean, msg: CreateOrderReply): CreateOrderReply.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CreateOrderReply, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CreateOrderReply;
    static deserializeBinaryFromReader(message: CreateOrderReply, reader: jspb.BinaryReader): CreateOrderReply;
}

export namespace CreateOrderReply {
    export type AsObject = {
        status: number,
        message: string,
        id: string,
        orderstatus: string,
        itemsList: Array<ItemOrder.AsObject>,
    }
}

export class GetOrdersReply extends jspb.Message { 
    getStatus(): number;
    setStatus(value: number): GetOrdersReply;
    getMessage(): string;
    setMessage(value: string): GetOrdersReply;
    clearDataList(): void;
    getDataList(): Array<Order>;
    setDataList(value: Array<Order>): GetOrdersReply;
    addData(value?: Order, index?: number): Order;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetOrdersReply.AsObject;
    static toObject(includeInstance: boolean, msg: GetOrdersReply): GetOrdersReply.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetOrdersReply, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetOrdersReply;
    static deserializeBinaryFromReader(message: GetOrdersReply, reader: jspb.BinaryReader): GetOrdersReply;
}

export namespace GetOrdersReply {
    export type AsObject = {
        status: number,
        message: string,
        dataList: Array<Order.AsObject>,
    }
}

export class GetOrderReply extends jspb.Message { 
    getStatus(): number;
    setStatus(value: number): GetOrderReply;
    getMessage(): string;
    setMessage(value: string): GetOrderReply;

    hasOrder(): boolean;
    clearOrder(): void;
    getOrder(): Order | undefined;
    setOrder(value?: Order): GetOrderReply;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetOrderReply.AsObject;
    static toObject(includeInstance: boolean, msg: GetOrderReply): GetOrderReply.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetOrderReply, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetOrderReply;
    static deserializeBinaryFromReader(message: GetOrderReply, reader: jspb.BinaryReader): GetOrderReply;
}

export namespace GetOrderReply {
    export type AsObject = {
        status: number,
        message: string,
        order?: Order.AsObject,
    }
}

export class UpdateOrderReply extends jspb.Message { 
    getStatus(): number;
    setStatus(value: number): UpdateOrderReply;
    getMessage(): string;
    setMessage(value: string): UpdateOrderReply;

    hasOrder(): boolean;
    clearOrder(): void;
    getOrder(): Order | undefined;
    setOrder(value?: Order): UpdateOrderReply;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): UpdateOrderReply.AsObject;
    static toObject(includeInstance: boolean, msg: UpdateOrderReply): UpdateOrderReply.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: UpdateOrderReply, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): UpdateOrderReply;
    static deserializeBinaryFromReader(message: UpdateOrderReply, reader: jspb.BinaryReader): UpdateOrderReply;
}

export namespace UpdateOrderReply {
    export type AsObject = {
        status: number,
        message: string,
        order?: Order.AsObject,
    }
}

export enum OrderStatus {
    PENDING_PAYMENT = 0,
    PENDING_APPROVAL = 1,
    COOKING = 2,
    SENDING = 3,
    FINISHED = 4,
}
