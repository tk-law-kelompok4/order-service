// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var cooking_pb = require('./cooking_pb.js');

function serialize_cooking_CookReply(arg) {
  if (!(arg instanceof cooking_pb.CookReply)) {
    throw new Error('Expected argument of type cooking.CookReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_cooking_CookReply(buffer_arg) {
  return cooking_pb.CookReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_cooking_CookRequest(arg) {
  if (!(arg instanceof cooking_pb.CookRequest)) {
    throw new Error('Expected argument of type cooking.CookRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_cooking_CookRequest(buffer_arg) {
  return cooking_pb.CookRequest.deserializeBinary(new Uint8Array(buffer_arg));
}


var CookingService = exports.CookingService = {
  cook: {
    path: '/cooking.Cooking/Cook',
    requestStream: false,
    responseStream: false,
    requestType: cooking_pb.CookRequest,
    responseType: cooking_pb.CookReply,
    requestSerialize: serialize_cooking_CookRequest,
    requestDeserialize: deserialize_cooking_CookRequest,
    responseSerialize: serialize_cooking_CookReply,
    responseDeserialize: deserialize_cooking_CookReply,
  },
};

exports.CookingClient = grpc.makeGenericClientConstructor(CookingService);
