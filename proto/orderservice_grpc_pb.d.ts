// package: orderservice
// file: orderservice.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "grpc";
import * as orderservice_pb from "./orderservice_pb";

interface IOrderServiceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    menu: IOrderServiceService_IMenu;
    createOrder: IOrderServiceService_ICreateOrder;
    getOrders: IOrderServiceService_IGetOrders;
    getOrder: IOrderServiceService_IGetOrder;
    updateOrder: IOrderServiceService_IUpdateOrder;
}

interface IOrderServiceService_IMenu extends grpc.MethodDefinition<orderservice_pb.MenuRequest, orderservice_pb.MenuReply> {
    path: "/orderservice.OrderService/Menu";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<orderservice_pb.MenuRequest>;
    requestDeserialize: grpc.deserialize<orderservice_pb.MenuRequest>;
    responseSerialize: grpc.serialize<orderservice_pb.MenuReply>;
    responseDeserialize: grpc.deserialize<orderservice_pb.MenuReply>;
}
interface IOrderServiceService_ICreateOrder extends grpc.MethodDefinition<orderservice_pb.CreateOrderRequest, orderservice_pb.CreateOrderReply> {
    path: "/orderservice.OrderService/CreateOrder";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<orderservice_pb.CreateOrderRequest>;
    requestDeserialize: grpc.deserialize<orderservice_pb.CreateOrderRequest>;
    responseSerialize: grpc.serialize<orderservice_pb.CreateOrderReply>;
    responseDeserialize: grpc.deserialize<orderservice_pb.CreateOrderReply>;
}
interface IOrderServiceService_IGetOrders extends grpc.MethodDefinition<orderservice_pb.GetOrdersRequest, orderservice_pb.GetOrdersReply> {
    path: "/orderservice.OrderService/GetOrders";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<orderservice_pb.GetOrdersRequest>;
    requestDeserialize: grpc.deserialize<orderservice_pb.GetOrdersRequest>;
    responseSerialize: grpc.serialize<orderservice_pb.GetOrdersReply>;
    responseDeserialize: grpc.deserialize<orderservice_pb.GetOrdersReply>;
}
interface IOrderServiceService_IGetOrder extends grpc.MethodDefinition<orderservice_pb.GetOrderRequest, orderservice_pb.GetOrderReply> {
    path: "/orderservice.OrderService/GetOrder";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<orderservice_pb.GetOrderRequest>;
    requestDeserialize: grpc.deserialize<orderservice_pb.GetOrderRequest>;
    responseSerialize: grpc.serialize<orderservice_pb.GetOrderReply>;
    responseDeserialize: grpc.deserialize<orderservice_pb.GetOrderReply>;
}
interface IOrderServiceService_IUpdateOrder extends grpc.MethodDefinition<orderservice_pb.UpdateOrderRequest, orderservice_pb.UpdateOrderReply> {
    path: "/orderservice.OrderService/UpdateOrder";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<orderservice_pb.UpdateOrderRequest>;
    requestDeserialize: grpc.deserialize<orderservice_pb.UpdateOrderRequest>;
    responseSerialize: grpc.serialize<orderservice_pb.UpdateOrderReply>;
    responseDeserialize: grpc.deserialize<orderservice_pb.UpdateOrderReply>;
}

export const OrderServiceService: IOrderServiceService;

export interface IOrderServiceServer {
    menu: grpc.handleUnaryCall<orderservice_pb.MenuRequest, orderservice_pb.MenuReply>;
    createOrder: grpc.handleUnaryCall<orderservice_pb.CreateOrderRequest, orderservice_pb.CreateOrderReply>;
    getOrders: grpc.handleUnaryCall<orderservice_pb.GetOrdersRequest, orderservice_pb.GetOrdersReply>;
    getOrder: grpc.handleUnaryCall<orderservice_pb.GetOrderRequest, orderservice_pb.GetOrderReply>;
    updateOrder: grpc.handleUnaryCall<orderservice_pb.UpdateOrderRequest, orderservice_pb.UpdateOrderReply>;
}

export interface IOrderServiceClient {
    menu(request: orderservice_pb.MenuRequest, callback: (error: grpc.ServiceError | null, response: orderservice_pb.MenuReply) => void): grpc.ClientUnaryCall;
    menu(request: orderservice_pb.MenuRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: orderservice_pb.MenuReply) => void): grpc.ClientUnaryCall;
    menu(request: orderservice_pb.MenuRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: orderservice_pb.MenuReply) => void): grpc.ClientUnaryCall;
    createOrder(request: orderservice_pb.CreateOrderRequest, callback: (error: grpc.ServiceError | null, response: orderservice_pb.CreateOrderReply) => void): grpc.ClientUnaryCall;
    createOrder(request: orderservice_pb.CreateOrderRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: orderservice_pb.CreateOrderReply) => void): grpc.ClientUnaryCall;
    createOrder(request: orderservice_pb.CreateOrderRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: orderservice_pb.CreateOrderReply) => void): grpc.ClientUnaryCall;
    getOrders(request: orderservice_pb.GetOrdersRequest, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrdersReply) => void): grpc.ClientUnaryCall;
    getOrders(request: orderservice_pb.GetOrdersRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrdersReply) => void): grpc.ClientUnaryCall;
    getOrders(request: orderservice_pb.GetOrdersRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrdersReply) => void): grpc.ClientUnaryCall;
    getOrder(request: orderservice_pb.GetOrderRequest, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrderReply) => void): grpc.ClientUnaryCall;
    getOrder(request: orderservice_pb.GetOrderRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrderReply) => void): grpc.ClientUnaryCall;
    getOrder(request: orderservice_pb.GetOrderRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrderReply) => void): grpc.ClientUnaryCall;
    updateOrder(request: orderservice_pb.UpdateOrderRequest, callback: (error: grpc.ServiceError | null, response: orderservice_pb.UpdateOrderReply) => void): grpc.ClientUnaryCall;
    updateOrder(request: orderservice_pb.UpdateOrderRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: orderservice_pb.UpdateOrderReply) => void): grpc.ClientUnaryCall;
    updateOrder(request: orderservice_pb.UpdateOrderRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: orderservice_pb.UpdateOrderReply) => void): grpc.ClientUnaryCall;
}

export class OrderServiceClient extends grpc.Client implements IOrderServiceClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public menu(request: orderservice_pb.MenuRequest, callback: (error: grpc.ServiceError | null, response: orderservice_pb.MenuReply) => void): grpc.ClientUnaryCall;
    public menu(request: orderservice_pb.MenuRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: orderservice_pb.MenuReply) => void): grpc.ClientUnaryCall;
    public menu(request: orderservice_pb.MenuRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: orderservice_pb.MenuReply) => void): grpc.ClientUnaryCall;
    public createOrder(request: orderservice_pb.CreateOrderRequest, callback: (error: grpc.ServiceError | null, response: orderservice_pb.CreateOrderReply) => void): grpc.ClientUnaryCall;
    public createOrder(request: orderservice_pb.CreateOrderRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: orderservice_pb.CreateOrderReply) => void): grpc.ClientUnaryCall;
    public createOrder(request: orderservice_pb.CreateOrderRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: orderservice_pb.CreateOrderReply) => void): grpc.ClientUnaryCall;
    public getOrders(request: orderservice_pb.GetOrdersRequest, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrdersReply) => void): grpc.ClientUnaryCall;
    public getOrders(request: orderservice_pb.GetOrdersRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrdersReply) => void): grpc.ClientUnaryCall;
    public getOrders(request: orderservice_pb.GetOrdersRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrdersReply) => void): grpc.ClientUnaryCall;
    public getOrder(request: orderservice_pb.GetOrderRequest, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrderReply) => void): grpc.ClientUnaryCall;
    public getOrder(request: orderservice_pb.GetOrderRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrderReply) => void): grpc.ClientUnaryCall;
    public getOrder(request: orderservice_pb.GetOrderRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: orderservice_pb.GetOrderReply) => void): grpc.ClientUnaryCall;
    public updateOrder(request: orderservice_pb.UpdateOrderRequest, callback: (error: grpc.ServiceError | null, response: orderservice_pb.UpdateOrderReply) => void): grpc.ClientUnaryCall;
    public updateOrder(request: orderservice_pb.UpdateOrderRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: orderservice_pb.UpdateOrderReply) => void): grpc.ClientUnaryCall;
    public updateOrder(request: orderservice_pb.UpdateOrderRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: orderservice_pb.UpdateOrderReply) => void): grpc.ClientUnaryCall;
}
