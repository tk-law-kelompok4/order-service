// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var orderservice_pb = require('./orderservice_pb.js');

function serialize_orderservice_CreateOrderReply(arg) {
  if (!(arg instanceof orderservice_pb.CreateOrderReply)) {
    throw new Error('Expected argument of type orderservice.CreateOrderReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_orderservice_CreateOrderReply(buffer_arg) {
  return orderservice_pb.CreateOrderReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_orderservice_CreateOrderRequest(arg) {
  if (!(arg instanceof orderservice_pb.CreateOrderRequest)) {
    throw new Error('Expected argument of type orderservice.CreateOrderRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_orderservice_CreateOrderRequest(buffer_arg) {
  return orderservice_pb.CreateOrderRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_orderservice_GetOrderReply(arg) {
  if (!(arg instanceof orderservice_pb.GetOrderReply)) {
    throw new Error('Expected argument of type orderservice.GetOrderReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_orderservice_GetOrderReply(buffer_arg) {
  return orderservice_pb.GetOrderReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_orderservice_GetOrderRequest(arg) {
  if (!(arg instanceof orderservice_pb.GetOrderRequest)) {
    throw new Error('Expected argument of type orderservice.GetOrderRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_orderservice_GetOrderRequest(buffer_arg) {
  return orderservice_pb.GetOrderRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_orderservice_GetOrdersReply(arg) {
  if (!(arg instanceof orderservice_pb.GetOrdersReply)) {
    throw new Error('Expected argument of type orderservice.GetOrdersReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_orderservice_GetOrdersReply(buffer_arg) {
  return orderservice_pb.GetOrdersReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_orderservice_GetOrdersRequest(arg) {
  if (!(arg instanceof orderservice_pb.GetOrdersRequest)) {
    throw new Error('Expected argument of type orderservice.GetOrdersRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_orderservice_GetOrdersRequest(buffer_arg) {
  return orderservice_pb.GetOrdersRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_orderservice_MenuReply(arg) {
  if (!(arg instanceof orderservice_pb.MenuReply)) {
    throw new Error('Expected argument of type orderservice.MenuReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_orderservice_MenuReply(buffer_arg) {
  return orderservice_pb.MenuReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_orderservice_MenuRequest(arg) {
  if (!(arg instanceof orderservice_pb.MenuRequest)) {
    throw new Error('Expected argument of type orderservice.MenuRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_orderservice_MenuRequest(buffer_arg) {
  return orderservice_pb.MenuRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_orderservice_UpdateOrderReply(arg) {
  if (!(arg instanceof orderservice_pb.UpdateOrderReply)) {
    throw new Error('Expected argument of type orderservice.UpdateOrderReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_orderservice_UpdateOrderReply(buffer_arg) {
  return orderservice_pb.UpdateOrderReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_orderservice_UpdateOrderRequest(arg) {
  if (!(arg instanceof orderservice_pb.UpdateOrderRequest)) {
    throw new Error('Expected argument of type orderservice.UpdateOrderRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_orderservice_UpdateOrderRequest(buffer_arg) {
  return orderservice_pb.UpdateOrderRequest.deserializeBinary(new Uint8Array(buffer_arg));
}


var OrderServiceService = exports.OrderServiceService = {
  menu: {
    path: '/orderservice.OrderService/Menu',
    requestStream: false,
    responseStream: false,
    requestType: orderservice_pb.MenuRequest,
    responseType: orderservice_pb.MenuReply,
    requestSerialize: serialize_orderservice_MenuRequest,
    requestDeserialize: deserialize_orderservice_MenuRequest,
    responseSerialize: serialize_orderservice_MenuReply,
    responseDeserialize: deserialize_orderservice_MenuReply,
  },
  createOrder: {
    path: '/orderservice.OrderService/CreateOrder',
    requestStream: false,
    responseStream: false,
    requestType: orderservice_pb.CreateOrderRequest,
    responseType: orderservice_pb.CreateOrderReply,
    requestSerialize: serialize_orderservice_CreateOrderRequest,
    requestDeserialize: deserialize_orderservice_CreateOrderRequest,
    responseSerialize: serialize_orderservice_CreateOrderReply,
    responseDeserialize: deserialize_orderservice_CreateOrderReply,
  },
  getOrders: {
    path: '/orderservice.OrderService/GetOrders',
    requestStream: false,
    responseStream: false,
    requestType: orderservice_pb.GetOrdersRequest,
    responseType: orderservice_pb.GetOrdersReply,
    requestSerialize: serialize_orderservice_GetOrdersRequest,
    requestDeserialize: deserialize_orderservice_GetOrdersRequest,
    responseSerialize: serialize_orderservice_GetOrdersReply,
    responseDeserialize: deserialize_orderservice_GetOrdersReply,
  },
  getOrder: {
    path: '/orderservice.OrderService/GetOrder',
    requestStream: false,
    responseStream: false,
    requestType: orderservice_pb.GetOrderRequest,
    responseType: orderservice_pb.GetOrderReply,
    requestSerialize: serialize_orderservice_GetOrderRequest,
    requestDeserialize: deserialize_orderservice_GetOrderRequest,
    responseSerialize: serialize_orderservice_GetOrderReply,
    responseDeserialize: deserialize_orderservice_GetOrderReply,
  },
  updateOrder: {
    path: '/orderservice.OrderService/UpdateOrder',
    requestStream: false,
    responseStream: false,
    requestType: orderservice_pb.UpdateOrderRequest,
    responseType: orderservice_pb.UpdateOrderReply,
    requestSerialize: serialize_orderservice_UpdateOrderRequest,
    requestDeserialize: deserialize_orderservice_UpdateOrderRequest,
    responseSerialize: serialize_orderservice_UpdateOrderReply,
    responseDeserialize: deserialize_orderservice_UpdateOrderReply,
  },
};

exports.OrderServiceClient = grpc.makeGenericClientConstructor(OrderServiceService);
